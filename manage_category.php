<?php

include './template/header.php';  

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="">
    <title>Inventory Management System</title>
</head>
<body>

  
  <br><br>
 
    <div class="container">
         <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Category</th>
                <th>Parent</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody id="get_category">
<!--
              <tr>
                <td>1</td>
                <td>Electronics</td>
                <td>Root</td>
                <td><a class="btn btn-success btn-sm" href="">Active</a></td>
                <td>
                    <a class="btn btn-danger btn-sm" href="">Delete</a>
                    <a class="btn btn-warning btn-sm" href="">Edit</a>
                </td>
              </tr>
-->
           
            </tbody>
          </table>
    </div>    
   
   
   <?php
        include_once('./template/update_category.php');
    
    ?>
   
   
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>