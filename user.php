
<?php

include 'connect.php';
include_once("DBOperation.php");
include_once('manage.php');
session_start();

//-------------register----------------




if(isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['usertype'])){
   
    $error = 0;
   
  
   
    
    $check = "select * from user where email='$_POST[email]' ";
    $rs = $conn->query($check);
    if($rs->num_rows>0){

        
        $error = 1;
        echo $error;
       
        
    }
    
    if($error==0){
       
        function check($x){
            global $conn;
            $removeSpace = trim($x);
            $removeSlash = stripslashes($removeSpace);
            $y = mysqli_real_escape_string($conn, $removeSlash);
            return $y;
            
        }
   
        
        
        $name = check($_POST['username']);
        $pass=check($_POST['password']);
        $mdpass =md5($pass);    
        $email = check($_POST['email']);
        $usertype = check($_POST['usertype']);
        $notes="";
        $date = date('Y-m-d');
       
        $userValue = "INSERT user set 
            username='$name',
            email='$email',
            password='$mdpass',
            usertype='$usertype',
            reg_date='$date',
            last_login='$date',
            notes='$notes' ";
    
    
      
        $conn->query($userValue);
        $last_id = $conn->insert_id; 
         
    
        $sqlSelect = "SELECT * FROM user WHERE id = '$last_id' ";
        $result = $conn->query($sqlSelect);
        
 
  }
    
}



//
//-------------login--------------
//


if(isset($_POST['log_email']) && isset($_POST['log_password'])){
        
        $email = $_POST['log_email'];
        $pass = md5($_POST['log_password']); 
        
        $sql = "select * from user where email='$email' and password='$pass'";
        
        $result = $conn->query($sql);
       
        
        if($result->num_rows>0){
             while($row = $result->fetch_assoc()){
                $_SESSION['userId']=$row['id'];
                $_SESSION['name']=$row['username'];
                $_SESSION['email']=$row['email'];
                $_SESSION['pass']=$row['password'];
                $_SESSION['last_login']=$row['last_login'];
                $_SESSION['usertype']=$row['usertype'];
//                echo "<script>document.location.href='profile.php'</script>";
                 $success = 1;
                 echo $success;
            }
            
           
            
        }
        
        }




//---------fetch category--------


if(isset($_POST['getCategory'])){
    $obj = new DBOperation();
    $rows = $obj->getRecord("category");
    foreach ($rows as $row){
        echo "<option value='".$row["cid"]."'>".$row['cat_name']."</option>";
    }
    exit();
}



//---------fetch brand--------


if(isset($_POST['getBrand'])){
    $obj = new DBOperation();
    $rows = $obj->getRecord("brand");
    foreach ($rows as $row){
        echo "<option value='".$row["bid"]."'>".$row['brand_name']."</option>";
    }
    exit();
}


//----------add category-------------
if(isset($_POST['cat_name']) && isset($_POST['parent_cat'])){
    $obj = new DBOperation();
    $result = $obj->addCatgeory($_POST['parent_cat'],$_POST['cat_name']);
    echo $result;
    exit();
}

//----------add brand-------------
if(isset($_POST['brand_name'])){
    $obj = new DBOperation();
    $result = $obj->addBrand($_POST['brand_name']);
    echo $result;
    exit();
}

//----------add product-------------
if(isset($_POST['product_name']) && isset($_POST['added_date'])){
    $obj = new DBOperation();
    $result = $obj->addProduct($_POST['select_cat'],$_POST['select_brand'],$_POST['product_name'],$_POST['product_price'],$_POST['product_qty'],$_POST['added_date']);
    echo $result;
    exit();
}


// ------------manage category----------

if(isset($_POST['manageCategory'])){
    $m = new Manage();
    $result = $m->manageRecordsWithPagination("category",1);
    $rows = $result['rows'];
    $pagination = $result['pagination'];
    
    if(count($rows)>0){
        $n = 0;
        foreach($rows as $row){
            ?>
            <tr>
                <td><?php echo ++$n; ?></td>
                <td><?php echo $row['category']; ?></td>
                <td><?php echo $row['parent']; ?></td>
                <td><a class="btn btn-success btn-sm" href="">Active</a></td>
                <td>
                    <a class="btn btn-danger btn-sm del_cat" href="" did="<?php echo $row['cid']; ?>">Delete</a>
                    <a  class="btn btn-warning btn-sm edt_cat" data-toggle="modal" data-target="#category" href="" eid="<?php echo $row['cid']; ?>">Edit</a>
                </td>
              </tr>
            <?php
        }
        ?>
            <tr><td colspan="5"><?php echo $pagination; ?></td></tr>
        <?php
        exit();
    }
    
}


//-----------delete category------------

if(isset($_POST['deleteCategory'])){
    $m = new Manage();
    $result = $m->deleteRecord("category","cid",$_POST['id']);
    echo $result;
}



//-----------edit category------------

if(isset($_POST['updateCategory'])){
    $m = new Manage();
    $result = $m->getSingleRecord("category","cid",$_POST['id']);
    echo json_encode($result);
    exit();
}

//-----------update category after getting data----------

if(isset($_POST['update_cat_name'])){
    $m = new Manage();
    $id = $_POST["cid"];
    $name = $_POST["update_cat_name"];
    $parent_cat = $_POST["parent_cat"];
    
    $result = $m->update_record("category",["cid"=>$id],["parent_cat"=>$parent_cat,"cat_name"=>$name,"status"=>1]);
   echo $result;
    exit();
}



/////////////////////////////////////
//---------ordering process----------
/////////////////////////////////////
if(isset($_POST['getNewOrderItem'])){
    
    $obj = new DBOperation();
   $rows = $obj->getRecord("product");

?>

    <tr>
        <td><b class="number">1</b></td>
        <td>
            <select name="pid[]" id="" class="form-control form-control-sm pid" required>
              <option value="">Choose product</option>
               <?php  
                
                foreach($rows as $row){
                    ?>
                    <option value="<?php echo $row['pid']; ?>"><?php echo $row['product_name']; ?></option>
                    
                    <?php
                }
                ?>
                
            </select>
        </td>
         <td>
            <input type="text" name="tqty[]" id="tqty[]" class="form-control form-control-sm tqty">
        </td>
        <td>
            <input type="text" name="qty[]" id="qty[]" class="form-control form-control-sm qty">
        </td>
       <td>
            <input type="text" name="price[]" id="price[]" class="form-control form-control-sm price" readonly>
        </td>
           
        
            <input type="hidden" name="pro_name[]" id="pro_name[]" class="form-control form-control-sm pro_name" >
        
        <td>Rs.<span class="amt">0</span></td>
    </tr>

<?php
    exit();
}


//-----------get price and qty-------------

if(isset($_POST['getPriceQty'])){
    $m = new Manage();
    $result = $m->getSingleRecord('product','pid',$_POST['id']);
    echo json_encode($result);
    exit();
}


if(isset($_POST['order_date']) AND isset($_POST['cust_name'])){
    $order_date = $_POST['order_date'];
    $cust_name = $_POST['cust_name'];
    
    $ar_tqty = $_POST['tqty'];
    $ar_qty = $_POST['qty'];
    $ar_price = $_POST['price'];
    $ar_pro_name = $_POST['pro_name'];
    
    $sub_total = $_POST['sub_total'];
    $gst = $_POST['gst'];
    $discount = $_POST['discount'];
    $net_total = $_POST['net_total'];
    $paid = $_POST['paid'];
    $due = $_POST['due'];
    $payment_type = $_POST['payment_type'];
    
    $m = new Manage();
     $result = $m->stroreCustomerOrderInvoice($order_date,$cust_name,$ar_tqty,$ar_qty,$ar_price,$ar_pro_name,$sub_total,$gst,$discount,$net_total,$paid,$due,$payment_type);
     echo json_encode($result);
    
}


?>

