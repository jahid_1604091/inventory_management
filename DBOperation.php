<?php

////////////////////////////////
//-------using class------------
///////////////////////////////


class DBOperation
{
    private $con;
    
    function __construct()
    {
        include_once("connection2.php");
        $db = new Database();
        $this->con = $db->connect();
    }
    
    public function addCatgeory($parent,$cat){
        $pre_stmt = $this->con->prepare("INSERT INTO `category`(`parent_cat`, `cat_name`, `status`) VALUES (?,?,?)");
        $status = 1;
        $pre_stmt->bind_param("isi",$parent,$cat,$status);
        $result = $pre_stmt->execute() or die($this->con->error);
        
        if($result){
            $success = 1;
            echo $success;
        }
        else{
            return 0;
        }
        
    }    
    
    
    public function addBrand($brand_name){
        $pre_stmt = $this->con->prepare("INSERT INTO `brand`(`brand_name`, `status`) VALUES (?,?)");
        $status = 1;
        $pre_stmt->bind_param("si",$brand_name,$status);
        $result = $pre_stmt->execute() or die($this->con->error);
        
        if($result){
            $success = 1;
            echo $success;
        }
        else{
            return 0;
        }
        
    }
    
    
    public function addProduct($cid,$bid,$pro_name,$price,$stock,$date){
    $pre_stmt = $this->con->prepare("INSERT INTO `product`(`cid`, `bid`, `product_name`, `product_price`, `product_stock`, `added_date`, `p_status`) VALUES (?,?,?,?,?,?,?)");
    $status = 1;
    $pre_stmt->bind_param("iisdisi",$cid,$bid,$pro_name,$price,$stock,$date,$status);
    $result = $pre_stmt->execute() or die($this->con->error);

    if($result){
        $success = 1;
        echo $success;
    }
    else{
        return 0;
    }

}

    
    public function getRecord($table){
        $pre_stmt = $this->con->prepare("SELECT * from ".$table);
        $pre_stmt->execute() or die($this->con->error);
        $result = $pre_stmt->get_result();
        $rows = array();
        if($result->num_rows>0){
            while($row = $result->fetch_assoc()){
                $rows[] = $row;
            }
            return $rows;
        }
        return "NO_DATA";
    }
}

//------------Add Category-------------
//$opr = new DBOperation();
//echo $opr->addCatgeory(1,"Mobiles");
//echo "<pre>";
//print_r($opr->getRecord("category"));

//------------Add Brand-------------
//$opr = new DBOperation();
//echo $opr->addBrand(1,"Mobiles");
//echo "<pre>";
//print_r($opr->getRecord("category"));







////////////////////////
//------Normal----------
////////////////////////

//include 'connect.php';
//
//$status = 1;
//$add = "INSERT INTO `category`(`parent_cat`, `cat_name`, `status`) VALUES (1,'Mobiles','$status')";
//$result = $conn->query($add);
//
//if($result){
//    return "category_added";
//}
//
//else{
//    return 0;
//}
//
//





?>