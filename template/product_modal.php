<!--Product Modal -->
<div class="modal fade" id="product" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Products</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="product_form" onsubmit="return false">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label>Date</label>
              <input type="text" class="form-control" id="added_date" name="added_date" value="<?php echo date('Y-m-d'); ?>">
            </div>
            <div class="form-group col-md-6">
              <label>Product Name</label>
              <input type="text" class="form-control" id="product_name" name="product_name">
            </div>
          </div>
          <div class="form-group">
            <label>Category</label>
            <select name="select_cat" id="select_cat" class="form-control" required>
                
                
            </select>
          </div>  
           
           <div class="form-group">
            <label>Brand</label>
            <select name="select_brand" id="select_brand" class="form-control" required>
                
                
            </select>
          </div>
            <div class="form-group">
              <label>Product price</label>
              <input type="text" class="form-control" id="product_price" name="product_price">
            </div>
            
            <div class="form-group">
              <label>Quantity</label>
              <input type="text" class="form-control" id="product_qty" name="product_qty">
            </div>
         
      
      <button type="submit" class="btn btn-success">Add Product</button>
</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>