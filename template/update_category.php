
<!--Update Category Modal -->
<div class="modal fade" id="category" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Update Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
             <form id="update_form_cat" onsubmit="return false">
                <div class="form-group">
                  <label>Category Name</label>
                  <input type="hidden" name="cid" id="cid" value="">
                  <input type="text" class="form-control" id="update_cat_name" name="update_cat_name">
                  <small id="cat_error" class="text-danger"></small>
             </div>   
                 
              <div class="form-group">
                  <label>Parent category</label>
                  <select name="parent_cat" id="parent_cat" class="form-control">
                     
                  </select>
                  
              </div>
              <button type="submit" class="btn btn-primary">Update</button>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>


