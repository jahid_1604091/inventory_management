<?php

include 'connect.php';
include './template/header.php';  


if(!isset($_SESSION['userId'])){
    header("location:index.php");
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="">
    <title>Inventory Management System</title>
</head>
<body>

  <!--       loader-->
      <div class="overlay"><div class="loader"></div></div>
  <br><br>
<div class="container">
   <div class="row">
       <div class="col-md-10 mx-auto">
           <div class="card">
              <div class="card-header">
                <h4>New Orders</h4>
              </div>
              <div class="card-body">
                <form id="get_order_data" onsubmit="return false" method="post">
                    <div class="form-group row">
                        <label class="col-sm-3">Order Date</label>
                        <div class="col-sm-6">
                            <input type="text" id="order_date" name="order_date" class="form-control form-control-sm" value="<?php echo date("Y-m-d"); ?>">
                        </div>   
                        
                    </div>   
                       
                    <div class="form-group row">
                        <label class="col-sm-3">Customer Name</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" placeholder = "Enter cutomer name" id="cust_name" name="cust_name"required>
                        </div>   
                        
                    </div>
                    
                    
                    <div class="card">
                        <div class="card-body">
                            <h3 class="text-center">Make a order list</h3> <hr>
                            <table width="800px">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item Name</th>
                                        <th>Total Quantity</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody id="invoice_item">
<!--
                                    <tr>
                                        <td><b id="number">1</b></td>
                                        <td>
                                            <select name="pid[]" id="" class="form-control form-control-sm">
                                                <option value="">Washing Machine</option>
                                            </select>
                                        </td>
                                         <td>
                                            <input type="text" name="tqty[]" class="form-control form-control-sm" readonly>
                                        </td>
                                        <td>
                                            <input type="text" name="qty[]" class="form-control form-control-sm">
                                        </td>
                                       <td>
                                            <input type="text" name="price[]" class="form-control form-control-sm" readonly>
                                        </td>
                                        <td>Rs.1540</td>
                                    </tr>
-->
                                </tbody>
                            </table>
                            
                            <center>
                            <button style="width:150px;" id="add" class="btn btn-success mt-2">Add</button>
                            <button style="width:150px;" id="remove" class="btn btn-danger mt-2">Remove</button></center>
                        </div>
                    </div>
                    
                    <p></p>
                      <div class="form-group row">
                        <label class="col-sm-3">Sub total</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm"  name="sub_total" id="sub_total">
                        </div>   
                        
                    </div>    
                       
                       <div class="form-group row">
                        <label class="col-sm-3">GST(18%)</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm"  name="gst" id="gst">
                        </div>   
                        
                    </div>    
                       
                       <div class="form-group row">
                        <label class="col-sm-3">Discount</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm"  name="discount" id="discount">
                        </div>   
                        
                    </div>    
                       
                       <div class="form-group row">
                        <label class="col-sm-3">Net total</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm"  name="net_total" id="net_total">
                        </div>   
                        
                    </div>    
                       
                    <div class="form-group row">
                        <label class="col-sm-3">Paid</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" name="paid" id="paid">
                        </div>   
                        
                    </div>     
                       
                     <div class="form-group row">
                        <label class="col-sm-3">Due</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" name="due" id="due">
                        </div>   
                        
                    </div>    
                       
                     <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Payment Method</label>
                        <div class="col-sm-6">
                            <select name="payment_type" id="payment_type" class="form-control form-control-sm" >
                                <option>Cash</option>
                                <option>Card</option>
                                <option>Draft</option>
                                <option>Cheque</option>
                            </select>
                        </div>   
                        
                    </div>    
                      
                        <center>
                            <input type="submit" style="width:150px;" id="order_form" class="btn btn-info mt-2" value="Order">
                            <input type="submit" style="width:150px;" id="print_invoice" class="btn btn-success mt-2 d-none" value="Print Invoice"></center>
                       
                   
                </form>
              </div>
            </div>
       </div>
   </div>



</div>
 
   
   
   
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/order.js"></script>
</body>
</html>