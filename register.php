<?php

include 'connect.php';
session_start();

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="">
    <title>Inventory Management System</title>
</head>
<body>
        <!--    nav bar-->
        
<?php  include './template/header.php';  ?>
  
  <div id="alert">
      
  </div>
  
  <br>
<div class="container">
      
    <div class="card mx-auto" style="width: 30rem;">
      <div class="card-header"><h2 class="text-center">Register</h2></div>
          <div class="card-body">

            <form onsubmit="return false" enctype="multipart/form-data" id="reg_form" autocomplete="off">
              
                <div class="form-group">
                  <label for="username">Full Name</label>
                  <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username">
                  <small id="name_error" class="text-danger form-text"></small>
                </div>   
                 
                <div class="form-group">
                  <label for="email">Email Address</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                  <small id="email_error" class="text-danger"></small>
                </div>   
                 
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                  <small id="p1_error" class="text-danger"></small>
                </div> 
                 
                <div class="form-group">
                  <label for="password2">Confirm password</label>
                  <input type="password" class="form-control" id="password2" name="cpass" placeholder="Re-enter password">
                  <small id="p2_error" class="text-danger"></small>
                </div>   
                 
                <div class="form-group">
                  <label for="usertype">Usertype</label>
                  <select name="usertype" id="usertype" class="form-control">
                     <option disabled>Select a type</option>
                      <option value="1">Admin</option>
                      <option value="0">Other</option>
                  </select>
                  <small id="type_error" class="text-danger"></small>
                </div>
               
              
               
                <button type="submit" name="submit" class="btn btn-primary" id="submit"><i class="fa fa-user-plus"></i>&nbsp;Register</button>
              <span><a href="index.php">Login</a></span>
        </form>


      </div>
         
    </div>

</div>
   
  

   
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>