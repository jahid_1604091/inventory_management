$(document).ready(function(){
    var domain = "http://localhost/inventory_management/src/";
   $('#reg_form').on("submit", function(){
       var error = 0;
       var name = $('#username').val();
       var email = $('#email').val();
       var password = $('#password').val();
       var password2 = $('#password2').val();
       var usertype = $('#usertype').val();
       
//       var e_patt = RegExp(/^[a-z0-9_-]+(\.[a-z0-9_-]+)*@[a-z0-9_-]+(\.[a-z0-9_-]+)*(\.[a-z]{2,4})$/);
       
      
       
       if(name=="" || name.length<6) {
           
           $('#username').addClass("border-danger");
           $('#name_error').html("Please enter at least six characters name");
           error+=1;
           
       }
        
     
       else{
            $('#username').removeClass("border-danger");
           $('#name_error').html("");
           
       }  
       
       
       
       if(email=="") {
           
           $('#email').addClass("border-danger");
           $('#email_error').html("Please enter a valid email address");
           error+=1;
           
       }
        
   
       else{
            $('#enail').removeClass("border-danger");
           $('#email_error').html("");
           
       }
       
       
       
       if(password=="") {
           
           $('#password').addClass("border-danger");
           $('#p1_error').html("Please enter password");
           error+=1;
           
       }
        
        else if(password.length<6){
            $('#password').addClass("border-danger");
            $('#p1_error').html("Password is weak!");
            error+=1;
            
       }
       
       else{
            $('#password').removeClass("border-danger");
           $('#p1_error').html("");
          
       }   
       
       if(password!=password2){
           $('#password').addClass("border-danger");
            $('#p2_error').html("Password mismatch!");
           error+=1;
       }
        else{
            $('#password2').removeClass("border-danger");
           $('#p2_error').html("");
           
       
       }  
       
       if(error==0) {
                
        $.ajax({
               
               url:'user.php',
               type:'post',
               data:{username:name,
                     email:email,
                     password:password,
                     usertype:usertype
                                },
               success:function(data){
                   if(data == 1){
                       alert("Already exist");
                   }
                   else{
                       window.location.href="index.php?msg=You are registered! You can login now";
                   }

              
               }
           });
          
          
       }
        
      
   });
    
    
    
    
//  ---------  Login part  -----------
    
    $('#log_form').on("submit",function(){
        
        
        var log_email = $('#log_email').val();
        var log_password = $('#log_password').val();
        var status = false;
        
        if(log_email=="") {
           
           $('#log_email').addClass("border-danger");
           $('#email_err').html("Please enter a valid email address");
           status = false;
           
       }
        
   
       else{
            $('#log_email').removeClass("border-danger");
           $('#email_err').html("");
           status = true;
           
       }
        
       if(log_password=="") {
           
           $('#log_password').addClass("border-danger");
           $('#pass_err').html("Please enter password");
            status = false;
           
       }
        
   
       else{
            $('#log_password').removeClass("border-danger");
           $('#pass_err').html("");
            status = true;
       }   
        
        if(status){
            $('.overlay').show();
                    $.ajax({
                        
               
               url:'user.php',
               type:'post',
               data:{
                     log_email:log_email,
                     log_password:log_password
                     
                                },
               success:function(data){
                   if(data==1){
                     $('.overlay').hide(); window.location.href="dashboard.php";
                   }
                   else{
                           $('.overlay').hide(); 
                       alert("Wrong");
                       console.log(data);
                   }
               }
           });
          
        }
    });
    
    
    
    //-----------fetch category--------
    fecth_category();
    function fecth_category(){
        $.ajax({
            url: 'user.php',
            method:'post',
            data:{getCategory:1},
            success:function(data){
                var root = "<option value='0'>Root</option>";
                var choose = "<option disabled>Choose Category</option>";
                $('#parent_cat').html(root+data);
                $('#select_cat').html(choose+data);
            }
        });
    }
    
    //-----------fetch brand--------
    fecth_brand();
    function fecth_brand(){
        $.ajax({
            url: 'user.php',
            method:'post',
            data:{getBrand:1},
            success:function(data){
                var choose = "<option>Choose brand</option>";
                $('#select_brand').html(choose+data);
                
            }
        });
    }
    
    
    //----------add category--------------
    
    $('#form_cat').on("submit",function(){
       if($('#cat_name').val()==""){
           $('#cat_name').addClass("border-danger");
           $('#cat_error').html("Please enter a name");
       }
        else{
            
            $.ajax({
                url: 'user.php',
                method:'post',
                data: $('#form_cat').serialize(),
                success:function(data){
                    if(data == 1){
                       
                        $('#cat_name').removeClass("border-danger");
              
                        $('#cat_error').html("<span class='text-success'>New category added succesfully !</span>");
                        fecth_category();
                    }
                    else{
                        alert(data);
                    }
                }
            });
        }
    });
    
    
    
   //----------add brand--------------
    
    $('#form_brand').on("submit",function(){
       if($('#brand_name').val()==""){
           $('#brand_name').addClass("border-danger");
           $('#brand_error').html("Please enter a name");
       }
        else{
            
            $.ajax({
                url: 'user.php',
                method:'post',
                data: $('#form_brand').serialize(),
                success:function(data){
                    if(data == 1){
                       
                        $('#brand_name').removeClass("border-danger");
              
                        $('#brand_error').html("<span class='text-success'>New brand added succesfully !</span>");
                        fecth_brand();
                    }
                    else{
                        alert(data);
                    }
                }
            });
        }
    });
    
    
    
//--------------add product------------------
$('#product_form').on("submit",function(){
         $.ajax({
                url: 'user.php',
                method:'post',
                data: $('#product_form').serialize(),
                success:function(data){
                    if(data == 1){
                         $('#product_name').val("");
                        $('#product_qty').val("");
                        alert("Product added succesfully !")
                    }
                    else{
                        
                        alert(data);
                    }
                }
            });
});

    
//----------------Manage category------------
    manageCategory();
    function manageCategory(){
        $.ajax({
                url: 'user.php',
                method:'post',
                data:{manageCategory:1},
                success:function(data){
                    $('#get_category').html(data);
                }
        });    
    }
    
    
    
    
//------delete category----------------
    $('body').delegate('.del_cat', 'click', function(){
        var did = $(this).attr('did');
        if(confirm('Are you sure you want to delete ?')){
            $.ajax({
               url: 'user.php',
                method: 'post',
                data : {deleteCategory:1, id:did},
                success: function(data){
                    if(data = 0){
                        alert('Sorry! You cant delete a dependent id');
                    }
                    else if(data = 1){
                        alert('Category deleted sucessfully!');
                    }
                    else if(data = 2){
                        alert('Deleted sucessfully!');
                    }
                    else{
                        alert(data);
                    }
                }
            });
        }
        else{
            
        }
    });
    
    
    //------edit category----------------
    
    
        fecth_category();
    function fecth_category(){
        $.ajax({
            url: 'user.php',
            method:'post',
            data:{getCategory:1},
            success:function(data){
                var root = "<option value='0'>Root</option>";
                $('#parent_cat').html(root+data);
                
            }
        });
    }
    
    
    
    
    $('body').delegate('.edt_cat','click', function(){
       var eid = $(this).attr('eid');
        $.ajax({
            url: 'user.php',
            method:'post',
            dataType:'json',
            data:{updateCategory:1, id:eid},
            success:function(data){
                console.log(data);
                $('#cid').val(data['cid']);
                $('#cat_name').val(data['cat_name']);
                $('#parent_cat').val(data['parent_cat']);
            }
        });
    });
    
    
    $('#update_form_cat').on('submit',function(){
        if($('#update_cat_name').val()==""){
           $('#update_cat_name').addClass("border-danger");
           $('#cat_error').html("Please enter a name");
       }
        else{
            
            $.ajax({
                url: 'user.php',
                method:'post',
                data: $('#update_form_cat').serialize(),
                success:function(data){
                    if(data == 1){
                       
                        $('#update_cat_name').removeClass("border-danger");
              
                        $('#cat_error').html("<span class='text-success'>New category added succesfully !</span>");
                        fecth_category();
                    }
                    else{
                        alert(data);
                        window.location.href="";
                    }
                }
            });
        }
    });
    
});
  