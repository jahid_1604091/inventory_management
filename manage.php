<?php


class Manage 
{
	
	private $con;
    
    function __construct()
    {
        include_once("connection2.php");
        $db = new Database();
        $this->con = $db->connect();
    }

    public function manageRecordsWithPagination($table,$pno){
    	$a = $this->pagination($this->con,$table,$pno,5);
    	if ($table == "category") {
    		$sql = "SELECT p.cat_name as category, c.cat_name as parent,p.cid,p.status from category p LEFT JOIN category c ON p.parent_cat = c.cid " .$a["limit"];
    	}
    	$result = $this->con->query($sql) or die($this->con->error);
    	$rows = array();
    	if ($result->num_rows>0) {
    		while ($row = $result->fetch_assoc()) {
    			$rows[] = $row;
    		}
    	}
    	return ["rows"=>$rows,"pagination"=>$a["pagination"]];
    }


    private function pagination($con, $table, $pno, $n){

    $query = $con->query("SELECT COUNT(*) as rows FROM ".$table);
    $row = mysqli_fetch_assoc($query);

    //$totalRecords = 500;
    $pageno = $pno;
    $numofRecordsPerPage = $n;
    
    $lastPage = $totalPage = ceil($row["rows"]/$numofRecordsPerPage);
    $pagination = "";

    if ($lastPage!=1) {
    	if ($pageno>1) {
    		// $previous = "";
    		$previous = $pageno-1;
    		$pagination .="<a href='pagination.php?pageno=".$previous."' style='color:red;'>".'Prev'."</a>";

    	}


    	for ($i=$pageno-5; $i <$pageno ; $i++) { 
    		if ($i>0) {
    			$pagination .="<a href='pagination.php?pageno=".$i."'>".$i."</a>";
    		}
    	}

    	$pagination .="<a href='pagination.php?pageno=".$pageno."'>".$pageno."</a>";

    	for ($i=$pageno+1; $i <=$lastPage ; $i++) { 
    		$pagination.="<a href='pagination.php?pageno=".$i."'>".$i."</a>";
    		if ($i>$pageno+4) {
    			break;
    		}
    	}

    	if ($lastPage > $pageno) {
    		$next = $pageno+1;
    		$pagination .="<a href='pagination.php?pageno=".$next."' style='color:red;'>".'Next'."</a>";
    	}

    }

    $limit = "LIMIT ".($pageno-1)*$numofRecordsPerPage.",".$numofRecordsPerPage;

    return ["pagination"=>$pagination,"limit"=>$limit];
}

    public function deleteRecord($table,$pk, $id){
        if($table == "category"){
            $pre_stmt = $this->con->prepare("select ".$id." from category where parent_cat= ?");
            $pre_stmt->bind_param("i",$id);
            $pre_stmt->execute();
            $result = $pre_stmt->get_result() or die($this->con->error);
            if($result->num_rows>0){
                return 0;
            }
            else{
                $pre_stmt = $this->con->prepare("delete from ".$table." where ".$pk." = ?  ");
                $pre_stmt->bind_param("i", $id);
                $result = $pre_stmt->execute() or die($this->con->error);
                if($result){
                    return 1;
                }
            }
        }
        else{
            $pre_stmt = $this->con->prepare("delete from ".$table." where ".$pk." = ?  ");
            $pre_stmt->bind_param("i", $id);
                $result = $pre_stmt->execute() or die($this->con->error);
                if($result){
                    return 2;
                }
        }
    }
    
    
    
    
    public function getSingleRecord($table, $pk, $id){
        $pre_stmt = $this->con->prepare("select * from ".$table." where ".$pk."=? ");
        $pre_stmt->bind_param("i", $id);
        $pre_stmt->execute() or die($this->con->error);
        $result = $pre_stmt->get_result();
        if($result->num_rows ==1 ){
            $row = $result->fetch_assoc();
        }
        return $row;
    }
    
    
    
     public function update_record($table,$where,$fields){
        $sql = "";
        $condition = "";
        foreach($where as $key=>$value){
            $condition .= $key ."='" . $value."' AND ";
        }
        $condition = substr($condition,0,-5);
        foreach($fields as $key=>$value){
            $sql .=$key. "='".$value."', ";
        }
        $sql = substr($sql,0,-2); //deleteing last comma(,)
        
        $sql = "update ".$table." set ".$sql." where ".$condition;
        //echo $sql;
        $query = mysqli_query($this->con,$sql);
        if($query){
            return "Updated";
        }
        
        
    }
    
    
    
public function stroreCustomerOrderInvoice($order_date,$cust_name,$ar_tqty,$ar_qty,$ar_price,$ar_pro_name,$sub_total,$gst,$discount,$net_total,$paid,$due,$payment_type){
  $pre_stmt = $this->con->prepare("INSERT INTO `invoice`(`customer_name`, `order_date`, `sub_total`, `gst`, `discount`, `net_total`, `paid`, `due`, `payment_type`) VALUES (?,?,?,?,?,?,?,?,?)");
        $pre_stmt->bind_param("ssdddddds", $cust_name, $order_date, $sub_total, $gst, $discount, $net_total, $paid, $due, $payment_type);
        $pre_stmt->execute() or die($this->con->error);
        $invoice_no = $pre_stmt->insert_id;
  
        
        if($invoice_no!=null){
            for($i=0; $i<count($ar_price); $i++){
                
                $rem_qty = $ar_tqty[$i] - $ar_qty[$i];
                if($rem_qty < 0){
                    return "Order Failed to complete!";
                }
                else{
                    $this->con->query("UPDATE `product` SET `product_stock`='$rem_qty' WHERE product_name= '".$ar_pro_name[$i]."'");
                }
                
                
                $insert_product = $this->con->prepare("INSERT INTO `invoice_details`(`invoice_no`, `product_name`, `price`, `qty`) VALUES (?,?,?,?)");
                $insert_product->bind_param("isdd",$invoice_no,$ar_pro_name[$i],$ar_price[$i],$ar_qty[$i]);
                $insert_product->execute() or die($this->con->error);
            }
            return 5;
        }
}
    

}


//$obj = new Manage();
////echo "<pre>";
////print_r($obj->getSingleRecord("category","cid",1));
//echo $obj->update_record("category",["cid"=>1],["parent_cat"=>0,"cat_name"=>"Electro","status"=>1]);

?>