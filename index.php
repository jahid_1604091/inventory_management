<?php

include 'connect.php';


if(isset($_SESSION['userId'])){
    header("location:dashboard.php");
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="">
    <title>Inventory Management System</title>
</head>
<body>
<!--       loader-->
      <div class="overlay"><div class="loader"></div></div>
       
        <!--    nav bar-->
    
<?php  include './template/header.php';  
    
    
?>

  <br>
  
<div class="container">
      <?php
    
    if(isset($_GET['msg'])){
        
    ?>
    
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <?php echo $_GET['msg']; ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php  
    }
    ?>
        
    <div class="card mx-auto" style="width: 18rem;">
      <img class="card-img-top mx-auto" src="img/user-login.png" alt="login" style="width:60%;">
      <div class="card-body">

           <form onsubmit="return false" enctype="multipart/form-data" id="log_form" autocomplete="off">
              <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" class="form-control" id="log_email" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                <small id="email_err" class="form-text text-danger"></small>
              </div>

             <div class="form-group">
                <label for="log_password">Password</label>
                <input type="password" class="form-control" id="log_password" placeholder="Password" name="password">
                <small id="pass_err" class="text-danger form-text"></small>
              </div>

              <button type="submit" class="btn btn-primary" name="signin" id="submit"><i class="fa fa-lock" ></i>&nbsp;Log in</button>
              <span><a href="register.php">Register</a></span>
        </form>


      </div>
          <div class="card-footer">
              <a href="#">Forgot password ?</a>
          </div>
          
          
    </div>
 

</div>
   
   
   
   
   
   
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>