<?php

include 'connect.php';

function pagination($conn, $table, $pno, $n){
    $query = $conn->query("SELECT COUNT(*) as rows FROM".$table);
    $row = mysqli_fetch_assoc($query);

    //$totalRecords = 500;
    $pageno = $pno;
    $numofRecordsPerPage = $n;
    
    $lastPage = $totalPage = ceil($row["rows"]/$numofRecordsPerPage);
    $pagination = "";

    if ($lastPage!=1) {
    	if ($pageno>1) {
    		// $previous = "";
    		$previous = $pageno-1;
    		$pagination .="<a href='pagination.php?pageno=".$previous."' style='color:red;'>".'Prev'."</a>";

    	}


    	for ($i=$pageno-5; $i <$pageno ; $i++) { 
    		if ($i>0) {
    			$pagination .="<a href='pagination.php?pageno=".$i."'>".$i."</a>";
    		}
    	}

    	$pagination .="<a href='pagination.php?pageno=".$pageno."'>".$pageno."</a>";

    	for ($i=$pageno+1; $i <=$lastPage ; $i++) { 
    		$pagination.="<a href='pagination.php?pageno=".$i."'>".$i."</a>";
    		if ($i>$pageno+4) {
    			break;
    		}
    	}

    	if ($lastPage > $pageno) {
    		$next = $pageno+1;
    		$pagination .="<a href='pagination.php?pageno=".$next."' style='color:red;'>".'Next'."</a>";
    	}

    }

    $limit = "LIMIT".($pageno-1)*$numofRecordsPerPage.",".$numofRecordsPerPage;

    return ["pagination"=>$pagination,"limit"=>$limit];
}


if (isset($_GET["pageno"])) {
	$pageno = $_GET["pageno"];
	$table = "";
	$array = pagination($conn,$table,$pageno,10);

	$sql = "SELECT * FROM".$table." ".$array["limit"];
	$query = $conn->query($sql);
	while ($row = mysqli_fetch_assoc($query)) {
		echo "<div>.$row[""].</div>";
	}

	echo $array["pagination"];
}



?>