-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2020 at 05:14 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `bid` int(11) NOT NULL,
  `brand_name` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`bid`, `brand_name`, `status`) VALUES
(1, 'Samsung', '1'),
(2, 'Nokia', '1');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `cid` int(11) NOT NULL,
  `parent_cat` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cid`, `parent_cat`, `cat_name`, `status`) VALUES
(1, 0, 'Electronics', '1'),
(4, 0, 'Software', '1'),
(7, 1, 'Mobiles', '1');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_no` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `order_date` date NOT NULL,
  `sub_total` double NOT NULL,
  `gst` double NOT NULL,
  `discount` double NOT NULL,
  `net_total` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `payment_type` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_no`, `customer_name`, `order_date`, `sub_total`, `gst`, `discount`, `net_total`, `paid`, `due`, `payment_type`) VALUES
(1, 'Jahid', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(2, 'ds', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(3, 'xvsd', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(4, 'sdvs', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(5, 'jtfg', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(6, 'jtfg', '0000-00-00', 8000, 1440, 10, 9430, 25, 9405, ''),
(7, 'jtfg', '0000-00-00', 8000, 1440, 0, 9440, 25, 9440, ''),
(8, '', '0000-00-00', 0, 0, 0, 0, 0, 0, ''),
(9, 'asfaw', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(10, 'xcbd', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(11, 'dge', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(12, 'dfg', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(13, 'fdg', '0000-00-00', 8000, 1440, 10, 9430, 20, 9410, ''),
(14, 'fdg', '0000-00-00', 6000, 1080, 0, 7080, 0, 7080, ''),
(15, 'gd', '0000-00-00', 6000, 1080, 0, 7080, 0, 7080, ''),
(16, 'dvs', '0000-00-00', 8000, 1440, 0, 9440, 0, 9440, ''),
(17, 'dvsd', '0000-00-00', 8000, 1440, 10, 9430, 0, 9430, ''),
(18, 'dsgjfgsjgfyegu', '2020-02-07', 14000, 2520, 10, 16510, 5000, 11510, ''),
(19, 'sndfbs', '2020-02-07', 8000, 1440, 0, 9440, 25, 9415, ''),
(20, 'xxxxxxx', '2020-02-07', 6000, 1080, 0, 7080, 0, 7080, ''),
(21, 'xxx', '2020-02-07', 6000, 1080, 0, 7080, 0, 7080, ''),
(22, 'dfbgsd', '2020-02-07', 6000, 1080, 0, 7080, 0, 7080, ''),
(23, 'Jahid', '2020-02-07', 8000, 1440, 0, 9440, 2500, 6940, ''),
(24, 'df', '2020-02-07', 30000, 5400, 0, 35400, 500, 34900, ''),
(25, 'sdvs', '2020-02-07', 30000, 5400, 0, 35400, 43, 35357, ''),
(26, 'sca', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(27, 'dcvsd', '2020-02-07', 6000, 1080, 0, 7080, 34, 7046, ''),
(28, 'sc', '2020-02-07', 6000, 1080, 0, 7080, 23, 7057, ''),
(29, 'zcs', '2020-02-07', 6000, 1080, 0, 7080, 34, 7046, ''),
(30, 'dfs', '2020-02-07', 6000, 1080, 0, 7080, 3, 7077, ''),
(31, 'sd', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(32, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 34, 7046, ''),
(33, 'ds', '2020-02-07', 6000, 1080, 0, 7080, 34, 7046, ''),
(34, 'df', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(35, 'efw', '2020-02-07', 6000, 1080, 0, 7080, 345, 6735, ''),
(36, 'efw', '2020-02-07', 6000, 1080, 0, 7080, 34, 7046, ''),
(37, 'sdf', '2020-02-07', 6000, 1080, 25, 7055, 25, 7030, ''),
(38, 'asf', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(39, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(40, 'fe', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(41, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(42, 'sdg', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(43, 'wdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(44, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(45, 'sdg', '2020-02-07', 6000, 1080, 0, 7080, 23, 7057, ''),
(46, 'erfg', '2020-02-07', 6000, 1080, 0, 7080, 45, 7035, ''),
(47, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(48, 'sdf', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(49, 'sdfsd', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(50, 'df', '2020-02-07', 6000, 1080, 0, 7080, 35, 7045, ''),
(51, 'sdg', '2020-02-07', 6000, 1080, 0, 7080, 235, 6845, ''),
(52, 'Jahid Hasan', '2020-02-07', 54000, 9720, 250, 63470, 500, 62970, ''),
(53, 'Mr Raju', '2020-02-07', 36000, 6480, 250, 42230, 5000, 37230, 'Cash'),
(54, 'Jahid', '2020-04-14', 16000, 2880, 0, 18880, 500, 18380, 'Cash'),
(55, 'vsdfv', '2020-04-14', 16000, 2880, 0, 18880, 600, 18280, 'Cash'),
(56, 'vwef', '2020-04-14', 8000, 1440, 0, 9440, 300, 9140, 'Cash'),
(57, 'Jahid', '2020-04-27', 20000, 3600, 20, 23580, 5000, 18580, 'Cash'),
(58, 'Mr X', '2020-04-27', 30000, 5400, 10, 35390, 5000, 30390, 'Cash'),
(59, 'Mr X', '2020-04-27', 14000, 2520, 20, 16500, 5000, 11500, 'Cash'),
(60, 'Mr X', '2020-04-27', 6000, 1080, 10, 7070, 500, 6570, 'Cash'),
(61, 'Mr Rafiq', '2020-04-27', 38000, 6840, 100, 44740, 5000, 39740, 'Cash'),
(62, 'Mr Raju', '2020-04-27', 38000, 6840, 500, 44340, 5000, 39340, 'Cash'),
(63, 'Mr', '2020-04-27', 16000, 2880, 0, 18880, 500, 18380, 'Cash'),
(64, 'dves', '2020-04-27', 8000, 1440, 0, 9440, 50, 9390, 'Cash'),
(65, 'Mr X', '2020-04-27', 30000, 5400, 100, 35300, 3000, 32300, 'Card');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_no`, `product_name`, `price`, `qty`) VALUES
(1, 17, 'Samsung Galaxy S8', 8000, 1),
(2, 18, 'Galaxy g6', 6000, 1),
(3, 18, 'Samsung Galaxy S8', 8000, 1),
(4, 19, 'Samsung Galaxy S8', 8000, 1),
(5, 21, 'Galaxy g6', 6000, 1),
(6, 22, 'Galaxy g6', 6000, 1),
(7, 23, 'Samsung Galaxy S8', 8000, 1),
(8, 24, 'Galaxy g6', 6000, 5),
(9, 25, 'Galaxy g6', 6000, 5),
(10, 26, 'Galaxy g6', 6000, 1),
(11, 27, 'Galaxy g6', 6000, 1),
(12, 28, 'Galaxy g6', 6000, 1),
(13, 29, 'Galaxy g6', 6000, 1),
(14, 30, 'Galaxy g6', 6000, 1),
(15, 31, 'Galaxy g6', 6000, 1),
(16, 32, 'Galaxy g6', 6000, 1),
(17, 33, 'Galaxy g6', 6000, 1),
(18, 34, 'Galaxy g6', 6000, 1),
(19, 35, 'Galaxy g6', 6000, 1),
(20, 36, 'Galaxy g6', 6000, 1),
(21, 37, 'Galaxy g6', 6000, 1),
(22, 38, 'Galaxy g6', 6000, 1),
(23, 39, 'Galaxy g6', 6000, 1),
(24, 40, 'Galaxy g6', 6000, 1),
(25, 41, 'Galaxy g6', 6000, 1),
(26, 42, 'Galaxy g6', 6000, 1),
(27, 43, 'Galaxy g6', 6000, 1),
(28, 44, 'Galaxy g6', 6000, 1),
(29, 45, 'Galaxy g6', 6000, 1),
(30, 46, 'Galaxy g6', 6000, 1),
(31, 47, 'Galaxy g6', 6000, 1),
(32, 48, 'Galaxy g6', 6000, 1),
(33, 49, 'Galaxy g6', 6000, 1),
(34, 50, 'Galaxy g6', 6000, 1),
(35, 51, 'Galaxy g6', 6000, 1),
(36, 52, 'Galaxy g6', 6000, 5),
(37, 52, 'Galaxy g6', 6000, 4),
(38, 53, 'Galaxy g6', 6000, 1),
(39, 53, 'Galaxy g6', 6000, 5),
(40, 54, 'Samsung Galaxy S8', 8000, 2),
(41, 54, '', 0, 0),
(42, 57, 'Samsung Galaxy S8', 8000, 1),
(43, 57, 'Galaxy g6', 6000, 2),
(44, 58, 'Galaxy g6', 6000, 5),
(45, 58, 'Samsung Galaxy S8', 8000, 0),
(46, 59, 'Galaxy g6', 6000, 1),
(47, 60, 'Galaxy g6', 6000, 1),
(48, 63, 'Samsung Galaxy S8', 0, 0),
(49, 65, 'Galaxy g6', 6000, 5);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `pid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `bid` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_price` double NOT NULL,
  `product_stock` int(11) NOT NULL,
  `added_date` date NOT NULL,
  `p_status` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`pid`, `cid`, `bid`, `product_name`, `product_price`, `product_stock`, `added_date`, `p_status`) VALUES
(1, 7, 1, 'Samsung Galaxy S8', 8000, 0, '0000-00-00', '1'),
(2, 1, 1, 'Galaxy g6', 6000, 441, '2020-01-09', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `usertype` enum('Admin','Other','','') NOT NULL,
  `reg_date` date NOT NULL,
  `last_login` datetime NOT NULL,
  `notes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `usertype`, `reg_date`, `last_login`, `notes`) VALUES
(1, 'Jahid Hasan', 'jh409780@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', '2020-01-05', '2020-01-05 00:00:00', 0),
(2, 'Hasana', 'h@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '2020-01-05', '2020-01-05 00:00:00', 0),
(3, 'Zahid Hasan', 'jh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Admin', '2020-01-05', '2020-01-05 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`bid`),
  ADD UNIQUE KEY `brand_name` (`brand_name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`cid`),
  ADD UNIQUE KEY `cat_name` (`cat_name`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_no`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_no` (`invoice_no`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pid`),
  ADD KEY `cid` (`cid`),
  ADD KEY `bid` (`bid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD CONSTRAINT `invoice_details_ibfk_1` FOREIGN KEY (`invoice_no`) REFERENCES `invoice` (`invoice_no`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `category` (`cid`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`bid`) REFERENCES `brand` (`bid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
