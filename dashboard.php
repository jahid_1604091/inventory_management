<?php

include 'connect.php';
include './template/header.php';  
include './template/category_modal.php';
include './template/product_modal.php';
include './template/brand_modal.php';

if(!isset($_SESSION['userId'])){
    header("location:index.php");
}

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="">
    <title>Inventory Management System</title>
</head>
<body>

  
  <br><br>
  <div class="container">
      <div class="row">
          <div class="col-md-4">
              <div class="card mx-auto">
                    <img class="card-img-top mx-auto" src="img/me1.jpg" alt="Card image cap" style=" width:60%;">
                  <div class="card-body">
                    <h5 class="card-title">Profile</h5>
                    <p class="card-text">Jahid Hasan</p>
                    <p class="card-text"><i class="fa fa-user-md"></i>&nbsp;Admin</p>
                    <p class="card-text"><i class="fa fa-clock-o"></i>&nbsp;Last Login</p>
                    <a href="#" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit Profile</a>
                  </div>
                </div>
          </div>
          
          
          <div class="col-md-8">
              <div class="jumbotron" style="width:100%; height:100%;">
                 <h1>Welcome Admin,</h1>
                  <div class="row">
                      <div class="col-sm-6">
                          <iframe src="http://free.timeanddate.com/clock/i73a54mk/n73/szw110/szh110/hoc009/hbw1/hfc555/cf100/hnc000/fan2/facfff/fnu3/fdi84/mqcfff/mqs4/mql18/mqw8/mqd62/mhcfff/mhs4/mhl9/mhw4/mhd62/mmv0/hhcfff/hhs2/hhl50/hhb0/hhw30/hhr16/hmcfff/hms2/hml70/hmb0/hmw16/hmr10/hss3/hsl70/hsb0/hsw11/hsr7" frameborder="0" width="110" height="110"></iframe>
                      </div>
                    <div class="col-sm-6">
                        <div class="card">
                          <div class="card-body">
                            <h5 class="card-title">New Orders</h5>
                            <p class="card-text">Here you can make invoices and make orders.</p>
                            <a href="new_order.php" class="btn btn-primary">New Orders</a>
                          </div>
                      </div>
                  </div>
                  
              </div>
          </div>
          
      </div>

      </div>  
  </div>
  
  <br>
  
  <div class="container">
     <div class="row">
      <div class="col-md-4">
           <div class="card">
              <div class="card-body">
                <h4 class="card-title">Categories</h4>
                <p class="card-text">Here you can manage your categories</p>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#category">Add</a>
                <a href="manage_category.php" class="btn btn-primary">Manage</a>
              </div>
          </div>
      </div>
      <div class="col-md-4">
           <div class="card">
              <div class="card-body">
                <h4 class="card-title">Brands</h4>
                <p class="card-text">Here you can manage your brands</p>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#brand">Add</a>
                <a href="#" class="btn btn-primary">Manage</a>
              </div>
          </div>
      </div>
      <div class="col-md-4">
           <div class="card">
              <div class="card-body" >
                <h4 class="card-title">Products</h4>
                <p class="card-text">Here you can manage your products</p>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#product">Add</a>
                <a href="#" class="btn btn-primary">Manage</a>
              </div>
          </div>
      </div>
      </div>
  </div>
   
   



   
   
   
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
</body>
</html>